import 'package:flutter/material.dart';
import 'package:todoapp/model/todo.dart';

import 'date_field.dart';

class CreateTodo extends StatefulWidget {
  final Function addList;

  const CreateTodo({this.addList});

  @override
  _CreateTodoState createState() => _CreateTodoState();
}

class _CreateTodoState extends State<CreateTodo> {
  final textController = TextEditingController();

  String dateStartDate;
  String dateEndDate;

  String callbackStartDate(DateTime startDate) {
    print('test+$startDate');
    setState(() {
      dateStartDate = startDate.toString();
    });
    return startDate.toString();
  }

  String callbackEndDate(DateTime endDate) {
    print('test+$endDate');
    setState(() {
      dateEndDate = endDate.toString();
    });
    return endDate.toString();
  }

  @override
  void initState() {
    super.initState();
    dateStartDate = DateTime.now().toString();
    dateEndDate = DateTime.now().toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add To-Do list', textAlign: TextAlign.start),
          backgroundColor: Color.fromRGBO(241, 187, 65, 1),
        ),
        body: Container(
          padding: EdgeInsets.all(50),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('To-Do Title',
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.start),
              Padding(
                padding: EdgeInsets.all(10),
              ),
              Container(
                height: 100,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.black),
                ),
                child: TextField(
                  cursorColor: Colors.black,
                  controller: textController,
                  decoration: InputDecoration(
                    hintText: 'Please key in your To-Do title here',
                    contentPadding: EdgeInsets.all(10),
                    border: InputBorder.none,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40, left: 10),
                child: Text('Start Date',
                    style: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start),
              ),
              Padding(
                padding: EdgeInsets.all(10),
              ),
              DateField(
                  onSelectDate: callbackStartDate, hint: 'Select Start Date'),
              Padding(
                padding: const EdgeInsets.only(top: 40, left: 10),
                child: Text('End Date',
                    style: TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start),
              ),
              Padding(
                padding: EdgeInsets.all(10),
              ),
              DateField(
                onSelectDate: callbackEndDate,
                hint: 'Select End Date',
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.black,
            child: RaisedButton(
              color: Colors.transparent,
              child: Text('Create Now',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
              onPressed: () {
                final todo = Todo(
                    endDate: dateEndDate,
                    startDate: dateStartDate,
                    title: textController.value.text);
                textController.clear();
                widget.addList(todo: todo);
                Navigator.of(context).pop();
              },
            )));
  }
}
